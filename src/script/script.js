"use strict";

document.addEventListener("DOMContentLoaded", setup);

let global = {
    stop: true,
    wpm: localStorage.getItem("wpm")
}

/**
 * @author Neil Fisher (1939668)
 * @author Castiel Le (1933080)
 * 
 * initializes global variables and adds event listeners
 */
function setup() {
    global.wordContainer = document.querySelector("#wordcontainer");
    global.stopbtn = document.querySelector(".stop");

    global.wordBefore = document.querySelector("#word > #before");
    global.wordFocus = document.querySelector("#word > .focus");
    global.wordAfter = document.querySelector("#word > #after");
    global.wordSpace = document.querySelector("#word > .space");

    global.fullQuote = document.querySelector("#fullquote");
    global.message = document.querySelector("#message");

    let inputWpm = document.querySelector(".wpm");
    inputWpm.addEventListener("input", updateWpm);
    if (global.wpm == null || global.wpm < 50 || global.wpm > 1000) {
        global.wpm = 100;
        inputWpm.value = 100;
    }
    else {
        inputWpm.value = global.wpm;
    }

    global.stopbtn.addEventListener("click", () => {
        toggleQuoteReading();
        if (global.stop === false) {
            getNextQuote();
        }
    });
}

/**
 * @author Neil Fisher (1939668)
 * @author Castiel Le (1933080)
 * 
 * sets the wpm that is stored in local storage & updates wpm global variable
 */
function updateWpm(e) {
    global.wpm = e.target.value;
    localStorage.setItem("wpm", e.target.value);
}

/**
 * @author Neil Fisher (1939668)
 * @author Castiel Le (1933080)
 * 
 * Uses fetch api to request a quote from the Ron Quotes url
 * @return {Array} a string array from the json() function that is called on the response object
 */
function getNextQuote() {
    fetch("https://ron-swanson-quotes.herokuapp.com/v2/quotes")
        .then(response => {
            if (response.ok)
                return response.json();
            throw new Error("Status code: " + response.status);
        })
        .then(data => {
            global.fullQuote.textContent = data[0];
            displayQuote(data[0].split(" "));
        })
        .catch(error => console.error("Error: " + error));
}

/**
 * @author Neil Fisher (1939668)
 * 
 * uses an interval to set the word displayed in the container.
 * interval time depends on the user-set wpm
 * @param {Array} words - array of strings representing the words of the quote to display
 */
function displayQuote(words) {
    // 60000ms in 1 min
    // 60000 / wpm = interval in ms
    global.message.textContent = null;
    let i = 0;
    let showWord = setInterval(() => {
        if (i < words.length && global.stop === false) {
            let word = words[i];
            displayWord(word);
            i++;
        }
        else {
            if (global.stop === false)
                toggleQuoteReading();
            global.stop = true;
            global.message.textContent = "Quote End";
            clearInterval(showWord);
        };
    }, 60000 / global.wpm);
}

/**
 * @author Castiel Le (1933080)
 * 
 * splits word into separate strings & centers the word in its container
 * @param {String} word - word from the quote to be split up to have a focus letter in a different colour
 */
function displayWord(word) {
    //use substring for putting string into before, focus, after
    //the "focus letter" is the center letter of the word
    const focusIndex = word.length / 2; //focus at center of word
    global.wordBefore.textContent = word.substring(0, focusIndex);
    global.wordFocus.textContent = word.substring(focusIndex, focusIndex + 1);
    global.wordAfter.textContent = word.substring(focusIndex + 1);
    if (word.length % 2 == 0) {
        global.wordSpace.classList.add("hide");
    }
    else {
        global.wordSpace.classList.remove("hide");
    }
}

/**
 * @author Neil Fisher (1939668)
 * @author Castiel Le (1933080)
 * 
 * toggles the stopped states
 * toggles the animation shown
 */
function toggleQuoteReading() {
    global.stop = !global.stop;
    global.stopbtn.value = global.stop === true ? "Start" : "Stop";

    document.querySelector("#endspace").classList.remove("hide");
    document.querySelector(".messagecontainer").classList.remove("hide");

    // container box shadow animation
    if (global.stop === true) { // is stopped?
        global.wordContainer.classList.toggle("stopped");
        global.wordContainer.classList.toggle("started");

        global.message.textContent = "Quote End";
    }
    else {
        global.wordContainer.classList.toggle("stopped");
        global.wordContainer.classList.toggle("started");
    }
}

